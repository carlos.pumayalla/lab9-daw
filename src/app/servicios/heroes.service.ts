
import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

  private heroes:Heroe[] = [
    {
      nombre: "Paolo Guerrero",
      bio: "Su equipo actual es SC Internacional de la Sería A del Brasileirao. Es internacional absoluto con la Selección de Perú desde el 2004, de la cual es capitán y máximo goleador histórico. Ha participado en la Mundial de Rusia 2018 y en la Copa América 2019, donde llegó a la final y terminó como goleador del torneo.",
      img: "assets/img/paolo.jpg",
      aparicion: "1984-01-1",
      posicion: "Delantero"
    },
    {
      nombre: "Christian Cueva",
      bio: "Actualmente juega en el Al-Fateh de la Liga Profesional Saudí. Es internacional absoluto con la selección de fútbol de Perú, de la que es el máximo asistente en actividad con 12 pases definitorios. Así mismo ha marcado 12 goles, siendo el cuarto goleador en actividad.",
      img: "assets/img/cueva.jpg",
      aparicion: "1991-11-23",
      posicion: "Mediocampista"
 
    },
    {
      nombre: "André Carrillo",
      bio: "Su equipo actual es el Al Hilal F. C. de la Liga Profesional Saudí de Arabia Saudita. Es internacional absoluto con la selección nacional de Perú desde 2011. Destaca por ser desequilibrante en el ataque, dotado de velocidad y dominio técnico, y caracterizado por sus amagues y gambetas.",
      img: "assets/img/carrillo.jpg",
      aparicion: "1991-06-14",
      posicion: "Extremo Derecho"
 
    },
    {
      nombre: "Pedro Gallese",
      bio: "Actualmente milita en el Orlando City de la MLS de Estados Unidos. Es el guardameta de la selección de fútbol del Perú desde 2014, donde tiene como preseas la medalla del subcampeonato en la Copa América 2019 y una medalla de bronce conseguida en la Copa América 2015.",
      img: "assets/img/gallese.jpg",
      aparicion: "1990-02-23",
      posicion: "Arquero"

    },
    {
      nombre: "Luis Advincula",
      bio: "Su equipo actual es Boca Juniors de la Primera División de Argentina. Es internacional con la selección peruana desde 2010, de la cual es el sexto jugador histórico con más presencias (100 partidos). Ha participado en 4 ediciones de la Copa América: Argentina 2011, Chile 2015, consiguiendo en ambas la medalla de bronce, una medalla de plata en Brasil 2019 y el cuarto lugar en Brasil 2021",
      img: "assets/img/advincula.jpg",
      aparicion: "1990-03-02",
      posicion: "Lateral Derecho"
    },
    {
      nombre: "Jefferson Farfán",
      bio: "Su equipo actual es Alianza Lima de la Liga 1 de Perú.​ Es el segundo máximo goleador histórico de la selección peruana de fútbol y es considerado por la prensa especializada como uno de los jugadores más importantes en la historia del fútbol peruano.",
      img: "assets/img/farfan.jpg",
      aparicion: "1984-10-26",
      posicion: "Mediapunta"

    },
    {
      nombre: "Yoshimar Yotún",
      bio: "Su equipo actual es el Cruz Azul de la Primera División de México. Es internacional absoluto con la Selección de Perú desde el 2011. Con la selección peruana, tiene como preseas: la medalla de plata del subcampeonato conseguida en la Copa América Brasil 2019 y dos medallas de bronce en las ediciones Argentina 2011 y Chile 2015. Es el segundo jugador con más presencias en la historia de su selección, con 106 partidos, y el segundo máximo asistente con 10 pases de gol.",
      img: "assets/img/yotun.jpg",
      aparicion: "1990-04-07",
      posicion: "Mediocampista"
    }
  ];

  constructor() {
    console.log("Servicio listo para usar!!!");
  }


  getHeroes():Heroe[]{
    return this.heroes;
  }

  getHeroe( idx: any ){
    return this.heroes[idx];
  }

  buscarHeroes( termino:string ):Heroe[]{

    let heroesArr:Heroe[] = [];
    termino = termino.toLowerCase();

    for( let i = 0; i < this.heroes.length; i ++ ){

      let heroe = this.heroes[i];

      let nombre = heroe.nombre.toLowerCase();

      if( nombre.indexOf( termino ) >= 0  ){
        heroe.idx = i;
        heroesArr.push( heroe )
      }

    }

    return heroesArr;

  }


}


export interface Heroe{
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  posicion: string;
  idx?: number;
};
